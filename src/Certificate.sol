// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "./Interface/IERC5192.sol";
import "./Interface/ICertificate.sol";
import { Error } from "./misc/Constants.sol";

contract Certificate is ERC721, AccessControl, ICertificate {

    bytes32 public constant OPERATOR = keccak256("OPERATOR");
    bytes32 public constant SPENDER = keccak256("SPENDER");

    event Locked(uint256 tokenId);
    event Unlocked(uint256 tokenId);
    event logRegistration(Citizen _cit);
    event logNewBid(address owner, CertificateForReceipt _cert);
    event logsubmissionNotification(address user, uint256 time, uint256 id);
    event logCertificateCreated(uint256 id, bool status, address operator, uint256 time, address user);
    event logChangeBalamce(uint256 id, uint256 amount, uint256 time);
    
    uint256 private _ids;

    mapping(uint256 => bool) _locked;
    mapping(address => Citizen) internal _users;
    mapping(uint256 => CertificateForReceipt) internal _certificates;


    modifier IsTransferAllowed(uint256 tokenId) {
        require(!_locked[tokenId], Error.NON_TRANSFER);
        _;
    }

    constructor(address defaultAdmin, address operator, address spender) ERC721("SocChainCertificate", "SOC") {
        _grantRole(DEFAULT_ADMIN_ROLE, defaultAdmin);
        _grantRole(OPERATOR, operator);
        _grantRole(SPENDER, spender);

    }

    function transferFrom(
        address from,
        address to, 
        uint256 tokenId
    ) public override(ERC721) IsTransferAllowed(tokenId) {
        super.transferFrom(from, to, tokenId);
    }

    function registration(Citizen memory _cit) external {
        require(_cit.citizen == msg.sender, Error.FALSE_CITIZEN);
        _users[msg.sender] = _cit;
        emit logRegistration(_cit);
    }

    function CreateBid() external {

        require(_users[msg.sender].citizen == address(0), Error.NOT_REGISTRATION);
        require(_users[msg.sender].citizen == msg.sender, Error.FALSE_CITIZEN);
        require(balanceOf(msg.sender) == 0, Error.RE_BID);

        CertificateForReceipt memory newBid;
        newBid.id = _ids;
        newBid.void = true;
        newBid.user = _users[msg.sender];

        _mint(msg.sender, _ids);
        _certificates[_ids] = newBid;
        ++_ids;

        emit logNewBid(msg.sender, newBid);

    }

    function certificateCreator(uint256 id, bool _status, CertificateForReceipt memory _certificate) external onlyRole(OPERATOR) {

        require(_certificates[id].void, Error.BID_NOT_EXIST);

        if (!_status) {
            delete _certificates[id];  
            _burn(id);
        }
        else {
            require(_certificate.user.citizen == _certificates[id].user.citizen);
            require(!_certificate.void, Error.BID_NOT_CANCEL);
            require(_certificate.status, Error.STATUS_UNAPPROVED);
            _certificates[id] = _certificate;
            emit logCertificateCreated(id, _status, msg.sender, block.timestamp, _certificate.user.citizen);
        }

    }

    function changeBalance(uint256 id, uint256 amount) public onlyRole(SPENDER) {
        require(_certificates[id].status, Error.STATUS_UNAPPROVED);
        require(_certificates[id].resources >= amount, Error.EXTRA_WASTE);
        _certificates[id].resources -= amount;
        emit logChangeBalamce(id, amount, block.timestamp);
    }

    function submissionNotification(uint256 id) external {
        require(_certificates[id].user.citizen == msg.sender, Error.FALSE_CITIZEN);
        emit logsubmissionNotification(msg.sender, block.timestamp, id);
    }


    function set_operator(address _comm) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _grantRole(OPERATOR, _comm);
    }

    function revork_operator(address _comm) external onlyRole(DEFAULT_ADMIN_ROLE) {
        revokeRole(OPERATOR, _comm);
    }


    function set_spender(address _comm) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _grantRole(SPENDER, _comm);
    }

    function revork_spender(address _comm) external onlyRole(DEFAULT_ADMIN_ROLE) {
        revokeRole(SPENDER, _comm);
    }

    //getter 
    
    function getInfoByCertificate(uint256 id) public view onlyRole(SPENDER) returns(CertificateForReceipt memory) {
        return _certificates[id];
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, AccessControl)
        returns (bool)
    {
        return interfaceId == type(IERC5192).interfaceId || super.supportsInterface(interfaceId);        
    }
}
