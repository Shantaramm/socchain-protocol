// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;


interface ICertificate {

    struct Citizen {
        address citizen;
        string surname;
        string name;
        string patronymic;
        string sex;
        uint256 dateBirth;
        string placeBirth;
    }

    struct actBirth {
        uint256 id;
        uint256 date_reg;
        uint256 dateBirth;
        string citizenship;
        string authority;   
    }

    struct Child {
        string surname;
        string name;
        string patronymic;
        string sex;
        uint256 SNILS;
        actBirth act;
    }

    struct Identification {
        address citizen;
        string documentType;
        string citizenship;
        string issued;
        uint256 seriesNumber;
        uint256 date;
    }

    struct CertificateForReceipt {
        uint256 id;
        Citizen user;
        bool void;
        bool status;
        Identification passport;
        uint256 SNILS;
        uint256 phoneNumber;
        Child child;
        bytes32 _documentation;
        uint256 resources;
    }

    function registration(Citizen memory ) external; 

    function CreateBid() external;

    function certificateCreator(uint256, bool, CertificateForReceipt memory) external;

    function changeBalance(uint256, uint256) external;

    function submissionNotification(uint256) external;

    function getInfoByCertificate(uint256) external returns(CertificateForReceipt memory);

    
}