// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;



library Error {
    string public constant NON_TRANSFER = "000";
    string public constant FALSE_CITIZEN = "001";
    string public constant NOT_REGISTRATION = "010";
    string public constant RE_BID = "011";
    string public constant BID_NOT_EXIST = "100";
    string public constant BID_NOT_CANCEL = "101";
    string public constant STATUS_UNAPPROVED = "110";
    string public constant EXTRA_WASTE = "111";
    string public constant ADDRESS_NOT_EXIST = "1000";

}