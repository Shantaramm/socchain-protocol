// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "./Interface/ICertificate.sol";
import { Error } from "./misc/Constants.sol";

contract CertificatePayments is AccessControl {

    using ECDSA for bytes32;

    bytes32 public constant COMMISSIONER = keccak256("COMMISSIONER");

    ICertificate public certificate;
    IERC20 public rubc;

    event logPayment(uint256 id, address recipient, uint256 amount, uint256 time);


    constructor (address _rubc, address defaultAdmin, address _comm) {
        rubc = IERC20(_rubc);
        _grantRole(DEFAULT_ADMIN_ROLE, defaultAdmin);
        _grantRole(COMMISSIONER, _comm);
    }

    function makePayments(
        uint256 id, 
        uint256 amount, 
        address recipient, 
        bytes memory user_sig, 
        string memory message
    ) external returns(bool) {

        ICertificate.CertificateForReceipt memory _certificate = certificate.getInfoByCertificate(id); 
        require(_certificate.user.citizen == getOwner(message, id, user_sig), Error.FALSE_CITIZEN);

        certificate.changeBalance(id, amount);
        require(recipient != address(0), Error.ADDRESS_NOT_EXIST);
        rubc.transfer(recipient, amount);
        emit logPayment(id, recipient, amount, block.timestamp);
        return true;
    }

    function set_certificate(address _cert) external onlyRole(DEFAULT_ADMIN_ROLE) {
        certificate = ICertificate(_cert);
    }

    function depositingFunds(uint256 _amount) external onlyRole(COMMISSIONER) {
        rubc.transferFrom(msg.sender, address(this), _amount);
    }

    function set_commissioner(address _comm) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _grantRole(COMMISSIONER, _comm);
    }

    function revork_commissioner(address _comm) external onlyRole(DEFAULT_ADMIN_ROLE) {
        revokeRole(COMMISSIONER, _comm);
    }

    function getOwner(
        string memory message,
        uint256 id,
        bytes memory _signature
    ) internal pure returns (address) {

        address owner = ECDSA.recover(keccak256(abi.encodePacked(message, id)), _signature);
        return owner;
    }   

}